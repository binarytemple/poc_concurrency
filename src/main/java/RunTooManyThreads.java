import java.util.concurrent.atomic.AtomicInteger;

public class RunTooManyThreads {

    public static void main(String[] argv) {
        final AtomicInteger count = new AtomicInteger(0);
        for (; ; ) {
            new Thread(new Runnable() {
                public void run() {
                    System.err.println("New thread #" + count.incrementAndGet());
                    while (true) {
                        try {
                            Thread.sleep(100);
                        } catch (Exception e) {
                            System.err.println(e);
                        }
                    }
                }
            }).start();
        }
    }
}
